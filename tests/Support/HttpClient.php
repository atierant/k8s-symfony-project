<?php

namespace App\Tests\Support;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;

trait HttpClient
{
    /**
     * @param array $server
     * @return KernelBrowser
     */
    public function createClient(array $server = []): KernelBrowser
    {
        $client = static::$container->get('test.client');
        $client->setServerParameters($server);

        return $client;
    }
}
