# k8s Symfony Project

## Tout devrait être fait dans la C.I. ...

## Dans le serveur bare-metal contenant k8s

### On configure le cluster en créant un nouveau namespace pour notre projet

`kubectl create namespace k8s-symfony-project`

### On ajoute le secret pour la db

`kubectl create -n k8s-symfony-project secret generic database-secret --from-literal=database=app --from-literal=password=fP8cz7Q63nV --from-literal=url=mysql://root:fP8cz7Q63nV@mysql:3306/app`

### On s'assure que l'ingress nginx (Reverse Proxy) est bien installé

`kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.35.0/deploy/static/provider/baremetal/deploy.yaml`

### Utilisation des images

Les images utilisées ont été buildées par la gitlab-ci et poussées sur le docker hub. Il ne reste plus qu'à installer le Chart Helm

`helm upgrade k8s-symfony-project helm --install --set-string phpfpm.env.plain.APP_ENV=prod,nginx.host=k8s-symfony-project.io,imageTag=5e3ce576 --namespace k8s-symfony-project`

